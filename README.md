# Long-term probabilistic hazard maps for phreatic eruptions at Vulcano island

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="40">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/csic.png?inline=false" width="80">

**Codes:** [OpenPDAC](https://gitlab.com/cheese5311126/codes/openpdac/openpdac-11)

**Target EuroHPC Architectures:** LUMI-C

**Type:** Hazard assessment

## Description

Phreatic eruptions are surface manifestations of the instability of a
volcano-hydrothermal system. They are often associated with the increase of
pressure and temperature in a shallow hydrothermal system fed by heat and gas
flux from a deeper magma source. The sudden decompression of the
hydrothermal fluids in the shallow, porous rocks might cause the fragmentation
of the rocks and the ejection of a mixture of gases (mostly water vapour), fine
(sub-millimeter) to coarse (mm) ash and lapilli (cm-sized) material, and large
(decimeter-sized) bombs. Phreatic explosions are relatively small in size (usually
<10⁵ m3 of ejected materials) compared to magmatic eruptions, but their impact
can be significant, especially on populated volcanic regions and on touristic
areas, such as volcanic islands (Montanaro et al., 2022). At Vulcano (Aeolian
islands, Italy), the recent 2021 hydrothermal unrest has raised awareness of the
need to assess the hazards associated with potential phreatic explosions. Fluid
dynamics modelling is a fundamental tool to characterise phreatic eruption
scenarios (i.e., identifying the controlling initial parameters and instability
conditions), to reconstruct past events, to define areas more likely impacted by
the eruptive products, and to communicate volcanic risk.
So far, the high computational cost of three-dimensional, multiphase flow
numerical models has hindered the possibility of a probabilistic assessment of
phreatic eruption hazards and of the quantification of uncertainty. Today
however the advent of exascale computing and the increased availability of
open-source computational fluid dynamic codes that can scale up to thousands of CPU cores opens new possibilities. It permits running relatively large (~
hundreds) ensembles of three-dimensional simulations to account for the
intrinsic (epistemic and aleatoric) uncertainty in the main input parameters
characterising the eruptive scenarios (Martinez Montesinos et al., 2022; Selva et
al., 2010; Tierz et al., 2021). In this Simulation Case, we address the multiphase
flow simulation of small-, medium- and large-magnitude phreatic eruption
scenarios at Vulcano. For each scenario, we account for the variability of some of
the main eruptive parameters (e.g., total solid and gas mass; initial pressure and
temperature) within prescribed ranges, to quantify the variability of the impacts
in a probabilistic way.

<img src="SC6.2.png" width="800">

**Figure 3.10.1.** Graphical abstract of SC6.2. Maps of percentages of ballistic impacts in
several scenarios of phreatic explosion at Vulcano. 3D benchmark simulations performed
with OpenPDAC flagship code (Giansante et al., submitted).

## Expected results

General open-source workflow and numerical engine for ensemble simulation of phreatic eruptions on
volcanic islands and probabilistic hazard maps in standard netCDF format provided to the stakeholders (Italian Civil
Protection competence centres). Guidelines for probabilistic hazard assessment from phreatic eruptions on volcanic islands
and assessment of multi-hazard implications.